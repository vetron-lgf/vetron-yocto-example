#!/bin/bash

PRJ_ROOT=${PWD}
OEROOT="${PRJ_ROOT}/source/poky"
BUILD_NAME="build-vetron-example"
ACCESS_KEY="/home/vagrant/.ssh/meta-vetron-keyfile"

META_VETRON_REMOTE=origin
BRANCH=pyro
META_VETRON_D="${PRJ_ROOT}/source/meta-vetron-community"
META_VETRON_RO_CMD="GIT_SSH_COMMAND=\"ssh -i ${ACCESS_KEY} -oStrictHostKeyChecking=no\"" 
META_VETRON_RO_URL="git@bitbucket.org:vetron-lgf/meta-vetron-community.git"
META_VETRON_GET_CMD="${META_VETRON_RO_CMD} git clone ${META_VETRON_RO_URL} -b ${BRANCH} ${META_VETRON_D}"
META_VETRON_UPDATE_CMD="${META_VETRON_RO_CMD} git -C ${META_VETRON_D} pull origin ${BRANCH}"

# Make a clone of the example project repo
[ ! -d "${PRJ_ROOT}" ] && git clone https://bitbucket.org/vetron-lgf/vetron-yocto-example ${PRJ_ROOT}

# SSH_CMD="ssh -A -i ${ACCESS_KEY} -oStrictHostKeyChecking=no"
# hg clone -e "${SSH_CMD}" "$2" "$3"


#urls for the layers in bblayers.conf. Make sure there is an equivalent directory in
# LAYER_DIR_LIST list.
LAYER_URL_LIST=( \
    "git://git.yoctoproject.org/poky" \
    "https://github.com/Freescale/meta-freescale" \
    "https://github.com/Freescale/meta-freescale-3rdparty" \
    "https://github.com/Freescale/meta-freescale-distro" \
)
LAYER_DIR_LIST=( \
    "${PRJ_ROOT}/source/poky" \
    "${PRJ_ROOT}/source/meta-freescale" \
    "${PRJ_ROOT}/source/meta-freescale-3rdparty" \
    "${PRJ_ROOT}/source/meta-freescale-distro" \
) 
for ((i = 0; i < ${#LAYER_URL_LIST[@]}; ++i));
do
    if [ ! -d ${LAYER_DIR_LIST[i]} ]
    then
        git clone ${LAYER_URL_LIST[i]} -b ${BRANCH} ${LAYER_DIR_LIST[i]}
    else
        git -C ${LAYER_DIR_LIST[i]} pull origin
    fi
done


##Get vetron sources or update
VETRON_DIR="${PRJ_ROOT}/source/meta-vetron-community"
if [ ! -d "${VETRON_DIR}" ]
then
    eval ${META_VETRON_GET_CMD}
else
    eval ${META_VETRON_UPDATE_CMD}
fi

cd $PRJ_ROOT

chmod 0700 -R "${PRJ_ROOT}/access-keys"

# Source the bitbake build Environment
set ${BUILD_NAME}
. ${OEROOT}/oe-init-build-env ${BUILD_NAME}

# Remove the default build files and replace with some of the prepared Vetron build files
rm ${PRJ_ROOT}/${BUILD_NAME}/conf/bblayers.conf ${PRJ_ROOT}/${BUILD_NAME}/conf/local.conf
cp ${PRJ_ROOT}/templates/bblayers.conf ${PRJ_ROOT}/${BUILD_NAME}/conf
cp ${PRJ_ROOT}/templates/local.conf ${PRJ_ROOT}/${BUILD_NAME}/conf


