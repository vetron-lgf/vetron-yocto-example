#!/bin/sh
#
# This script is called by Vagrant and is provisioned inthe Vagrantfile


#$1 $2 $3 are parameters passed in from the Vagrantfile
PRJ_ROOT="${PWD}"

ACCESS_KEY="/home/vagrant/.ssh/meta-vetron-keyfile"

META_VETRON_REMOTE=origin
META_VETRON_BRANCH=pyro
META_VETRON_D=meta-vetron-community
META_VETRON_RO_CMD="GIT_SSH_COMMAND=\"ssh -i ${ACCESS_KEY} -oStrictHostKeyChecking=no\"" 
META_VETRON_RO_URL="git@bitbucket.org:vetron-lgf/meta-vetron-community.git"
META_VETRON_GET_CMD="${META_VETRON_RO_CMD} git clone ${META_VETRON_RO_URL} -b ${META_VETRON_BRANCH} ${META_VETRON_D}"
META_VETRON_UPDATE_CMD="${META_VETRON_RO_CMD} git -C ${META_VETRON_D} pull ${META_VETRON_RO_REMOTE} ${META_VETRON_BRANCH}"

chown -R vagrant:vagrant ${PRJ_ROOT}
chmod -R 770 ${PRJ_ROOT}
chmod 0700 ${ACCESS_KEY}
# SSH_CMD="ssh -A -i ${ACCESS_KEY} -oStrictHostKeyChecking=no"
# hg clone -e "${SSH_CMD}" "$2" "$3"

cd "$PRJ_ROOT/source"

# get metadata sources 
[ ! -d "${PRJ_ROOT}/source/poky" ] && git clone git://git.yoctoproject.org/poky
[ ! -d "${PRJ_ROOT}/source/meta-freescale" ] && git clone https://github.com/Freescale/meta-freescale 
[ ! -d "${PRJ_ROOT}/source/meta-freescale-3rdparty" ] && git clone https://github.com/Freescale/meta-freescale-3rdparty 
[ ! -d "${PRJ_ROOT}/source/meta-freescale-distro" ] && git clone https://github.com/Freescale/meta-freescale-distro 
[ ! -d "${PRJ_ROOT}/source/meta-vetron-community" ] && eval ${META_VETRON_GET_CMD}

cd $PRJ_ROOT
 
chmod 775 -R ${PRJ_ROOT}
chmod 0700 "${PRJ_ROOT}/access-keys"


