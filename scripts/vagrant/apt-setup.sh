#!/usr/bin/env sh

export DEBIAN_FRONTEND=noninteractive

#configure timezone and locale

echo "America/Los_Angeles" > /etc/timezone && \
    dpkg-reconfigure -f noninteractive tzdata && \
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    sed -i -e 's/# en_AU.UTF-8 UTF-8/en_AU.UTF-8 UTF-8/' /etc/locale.gen && \
    echo 'LANG="en_US.UTF-8"'>/etc/default/locale && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8


grep -rnw "${1}" /etc/apt/sources.list
RETVAR=$? 
if [ "$RETVAR" -gt 0 ] ;
then
    echo "$1" | sudo tee -a /etc/apt/sources.list
else
    echo "Did not add BACKPORTS to SOURCES.LIST"
fi

unset RETVAR

BACKPORT_PKGS="git"

PKGS="vim ssh subversion mercurial build-essential gcc-multilib autoconf\
    automake groff libtool debhelper sshpass ccache pkg-config gawk\
    smbclient curl whois syslinux genisoimage unzip texinfo wget\
    diffstat chrpath cpio socat libsdl1.2-dev xterm bc sudo python python3\
    python3-pip python3-pexpect xz-utils debianutils iputils-ping tmux zsh\
    dos2unix\
"

apt-get update
apt-get -y -t jessie-backports install ${BACKPORT_PKGS}
apt-get install -y ${PKGS}

#Install oh-my-zsh addons
DIR=/home/vagrant/.oh-my-zsh
[ ! -d "$DIR" ] && \
	git clone git://github.com/robbyrussell/oh-my-zsh.git $DIR && \
	cp $DIR/templates/zshrc.zsh-template /home/vagrant/.zshrc && \
	chsh -s /bin/zsh vagrant && \
        chown -R vagrant:vagrant /home/vagrant/.oh-my-zsh && \
        chown -R vagrant:vagrant /home/vagrant/.zshrc

unset DIR


#Install fonts
DIR=/opt/share/powerline/fonts
[ ! -d "$DIR" ]  && \
	git clone https://github.com/powerline/fonts $DIR && \
	$DIR/install.sh && \
        chown -R vagrant:vagrant $DIR
unset DIR


