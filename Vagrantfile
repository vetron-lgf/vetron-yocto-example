# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "vetron-lgf/cirrus-yocto"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder ".", "/vagrant", disabled: true
  

  config.vm.provider "virtualbox" do |virtualbox|
        virtualbox.gui = false
        virtualbox.name = "vetron-example-cirrus-yocto"
        virtualbox.memory = 2048
        virtualbox.cpus = 2
  end


  # disable the default sync
  config.vm.synced_folder ".", "/vagrant", disabled: true
  # enable the project directory to be synced to the home folder
  
  #
  config.vm.synced_folder "./binary/.", "/vagrant/binary/"
  # config.vm.synced_folder ".", "/home/vagrant/vetron-yocto-example", SharedFoldersEnableSymlinksCreate: FALSE


  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL

  #configure the Read Only SSH access key for the repo to be available for the vagrant provisioner script
  config.vm.provision "file", source: "./access-keys/meta-vetron-keyfile", destination: "/home/vagrant/.ssh/meta-vetron-keyfile" 

  aptMirror = "http://debian.mirror.digitalpacific.com.au/debian/"
  projectName = "vetron-yocto-example"

  # install packages
  config.vm.provision "shell" do |s|
        s.path = "./scripts/vagrant/apt-setup.sh"
        s.args = ["deb #{aptMirror} jessie-backports main contrib"]
  end
  
  
  config.vm.provision :shell do |shell|
      shell.inline = "[ ! -d /home/vagrant/#{projectName} ] && git clone https://bitbucket.org/vetron-lgf/#{projectName}"
  end
  
  config.vm.provision "shell" do |s| 
        s.path = "./scripts/vagrant/vagrant-setup.sh"
  end
  
  
  
  # s.args contains the parameters for the shell provisioner that reclones the repo inside the vm
  # $1 = location of the READ only access key for the repo
  # $2 = the ssh repo to clone
  # $3 = the destination path to clone the repo into
  #config.vm.provision "shell" do |s| 
  # 	s.path = "./scripts/vagrant/clone-repos.sh"
  #     s.args = ["/home/vagrant/.ssh/meta-vetron-keyfile", "ssh://hg@bitbucket.org/vetron-lgf/meta-vetron-keyfile", "/home/vagrant/vetron-yocto-example"]
  #end
  
  # config.vm.synced_folder "./", "/home/vagrant/vetron-yocto-example"
  #config.vm.provision :shell do |shell|
  #    shell.inline = "sudo mount -t vboxsf -o uid=`id -u vagrant`,gid=`id -g vagrant` binary /vagrant/binary"
  #end
  
end
